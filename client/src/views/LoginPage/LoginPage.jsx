import React, { Component } from "react";
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Switch, Redirect} from 'react-router'
// nodejs library to set properties for components
import PropTypes from "prop-types";
import TwitterLogin from 'react-twitter-auth';
// redux actions
import * as userAction from '../../action/userAction';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Admin from "../../layouts/Admin.jsx"
import Header from "../../components/Header/Header.jsx";
import HeaderLinks from "../../components/Header/HeaderLinks.jsx";
import GridContainer from "../../components/Grid/GridContainer.jsx";
import GridItem from "../../components/Grid/GridItem.jsx";
import Card from "../../components/Card/Card.jsx";
import CardHeader from "../../components/Card/CardHeader.jsx";

import loginPageStyle from "../../assets/jss/material-dashboard-react/views/loginPage.jsx";

import image from "../../assets/img/bg7.jpg";

const userStatus = true;
var userData = {};
var userToken = '';

class LoginPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      cardAnimaton: "cardHidden",
    };
  }
  componentDidMount() {
    setTimeout(
      function() {
        this.setState({ cardAnimaton: "" });
      }.bind(this),
      700
    );
  }

  redirect = () => {
    this.props.userAction.setToken(userToken)
    this.props.userAction.setUserEmail(userData.email)
    this.props.userAction.setUserId(userData.id)
    this.props.userAction.setTwitterId(userData.twitterProvider.id)
    this.props.userAction.setTwitterToken(userData.twitterProvider.token)
    this.props.userAction.setTwitterSecretToken(userData.twitterProvider.tokenSecret)
    this.props.userAction.setSelectUser(userStatus)
  }

  onSuccess = (response) => {
    const token = response.headers.get('x-auth-token');
    response.json().then(user => {
      if (token) {
        userData = user;
        userToken = token;
      }
      setTimeout(()=>{
        this.redirect();
      }, 2000)
    });
  };

  onFailed = (error) => {
    alert(error);
  };

  render() {
    const { classes, ...rest } = this.props;
    return (
      this.props.userSelected !== true
      ?
      <div>
        <Header
          absolute
          color="transparent"
          brand="Twitter Helpdesk"
          rightLinks={<HeaderLinks />}
          {...rest}
        />
        <div
          className={classes.pageHeader}
          style={{
            backgroundImage: "url(" + image + ")",
            backgroundSize: "cover",
            backgroundPosition: "top center"
          }}
        >
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={4}>
                <Card className={classes[this.state.cardAnimaton]} style={{background:'none',}}>
                  <form>
                    <CardHeader color="primary" className={classes.cardHeader}>
                      <TwitterLogin 
                          loginUrl="http://localhost:4000/api/v1/auth/twitter"
                          onFailure={this.onFailed} onSuccess={this.onSuccess}
                          requestTokenUrl="http://localhost:4000/api/v1/auth/twitter/reverse"
                          showIcon={true}
                          style={{backgroundColor:'transparent',border:'none',fontSize:16,color:'white'}}
                        >
                        Login | Sign Up using <i className={"fab fa-twitter"} />
                      </TwitterLogin>
                    </CardHeader>
                  </form>
                </Card>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
      :
      <Switch>
        <Redirect from="/" to="/user"/>
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  console.log(state.user)
  return {
    userSelected:state.user.userSelected,
    userEmail:state.user.userEmail,
    userId:state.user.userId,
    userTweetId:state.user.userTweetId,
    userTweetToken:state.user.userTweetToken,
    userTweetSecret:state.user.userTweetSecret,
    token:state.user.token,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    userAction: bindActionCreators(userAction, dispatch)
  }
};


LoginPage.propTypes = {
  classes: PropTypes.object
};


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(loginPageStyle)(LoginPage));
