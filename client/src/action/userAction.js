import constant from '../constants/index'

var authToken = '';

export function setSelectUser(user){
  return dispatch => 
    dispatch({
        type:constant.SET_SELECTED_USER,
        payload:user
    })
}

export function setUserEmail(email){
  return dispatch => 
    dispatch({
        type:constant.USER_EMAIL,
        payload:email
    })
}

export function setUserId(id){
  return dispatch => 
    dispatch({
        type:constant.USER_ID,
        payload:id
    })
}

export function setTwitterId(tweetid){
  return dispatch => 
    dispatch({
        type:constant.TWITTER_ID,
        payload:tweetid
    })
}

export function setTwitterToken(tweettoken){
  return dispatch => 
    dispatch({
        type:constant.TWITTER_TOKEN,
        payload:tweettoken
    })
}

export function setTwitterSecretToken(tweetsecrettoken){
  return dispatch => 
    dispatch({
        type:constant.TWITTER_SECRET_TOKEN,
        payload:tweetsecrettoken
    })
}

export function setToken(token){
  authToken = token;
  return dispatch => 
  setGlobalToken(authToken, dispatch)
}

function setGlobalToken(token, dispatch){
  dispatch({
      type:constant.SET_TOKEN,
      payload:token
  })
}