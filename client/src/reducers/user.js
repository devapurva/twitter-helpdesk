import constant from '../constants'
const initialState = {
    userSelected:'',
    userEmail:'',
    userId:'',
    userTweetId:'',
    userTweetToken:'',
    userTweetSecret:'',
    token:'',
}

export default function userReducer(currentState, { type, payload }) {
  const state = currentState || initialState;
  switch (type) {

    case constant.SET_SELECTED_USER:
        return Object.assign({}, state, {
            userSelected : payload
        });
    
    case constant.USER_EMAIL:
        return Object.assign({}, state, {
            userEmail : payload
        });

    case constant.USER_ID:
        return Object.assign({}, state, {
            userId : payload
        });
    
    case constant.TWITTER_ID:
        return Object.assign({}, state, {
            userTweetId : payload
        });
    
    case constant.TWITTER_TOKEN:
        return Object.assign({}, state, {
            userTweetToken : payload
        });
    
    case constant.TWITTER_SECRET_TOKEN:
        return Object.assign({}, state, {
            userTweetSecret : payload
        });
        

    case constant.SET_TOKEN:
        return Object.assign({}, state, {
            token : payload
        });

    default:
    return state;
    }
  }
  
