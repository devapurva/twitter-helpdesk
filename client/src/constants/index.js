export default {
  SET_SELECTED_USER: 'SET_SELECTED_USER',
  SET_TOKEN: 'SET_TOKEN',
  USER_EMAIL: 'USER_EMAIL',
  USER_ID: 'USER_ID',
  TWITTER_ID: 'TWITTER_ID',
  TWITTER_TOKEN: 'TWITTER_TOKEN',
  TWITTER_SECRET_TOKEN: 'TWITTER_SECRET_TOKEN',
}