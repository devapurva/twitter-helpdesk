import React, { Component } from 'react'
import PropTypes from 'prop-types'
import SocialLogin from 'react-social-login'
import Button from '../CustomButtons/Button'

class SocialButton extends Component {

  static propTypes = {
    triggerLogin: PropTypes.func.isRequired,
    triggerLogout: PropTypes.func.isRequired
  }

  render () {
    const { children, triggerLogin, triggerLogout, ...props } = this.props

    return (
        <Button
          justIcon
          color="transparent"
          onClick={triggerLogin}
          {...props}
        >
          { children }
        </Button>
    )
  }
}

export default SocialLogin(SocialButton);
