import React,{Component} from 'react'
import store from '../../src/store.js'
import { Provider } from 'react-redux';
import { Router, Route, Switch, Redirect } from 'react-router'
import { createBrowserHistory } from 'history'

import Admin from "../layouts/Admin.jsx";
import LoginPage from '../views/LoginPage/LoginPage.jsx';

import "../assets/css/material-dashboard-react.css?v=1.7.0";

class App extends Component {
   
  render(){ 
      return ( 
            <Provider store={store}>
                <Router history={createBrowserHistory()} >
                   <Switch >
                        <Route path="/user" component={Admin} />
                        <Route path="/" component={LoginPage} />
                        <Redirect from="/user" to="/user/dashboard" />
                   </Switch>
                </Router>                      
            </Provider>
        )
    }
}

export default (App);